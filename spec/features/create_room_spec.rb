require "rails_helper"

RSpec.feature "Users can create new room" do

  scenario "with valid attributes" do
    visit "/"
    click_link "New Room"

    select "A", from: "Area"
    fill_in "Name", with: "101"
    fill_in "Capacity", with: "30"
    fill_in "Description", with: "This is a description"
    
    click_button "Create Room"
    expect(page).to have_content "Room has been created."
  end
end