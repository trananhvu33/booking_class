class Reservation < ActiveRecord::Base
  belongs_to :user
  belongs_to :room

  # validates :time_end, numericality: { greater_than: :time_start }
  
  STATES = %w{ active inactive }

  STATES.each do |state|
    define_method("#{state}?") do 
      self.state == state
    end

    define_method("#{state}!") do
      self.update_attribute(:state, state)
    end
  end
end
