class Room < ActiveRecord::Base

  @allow_value = %w(A B C D E F)
  validates :name, uniqueness: { scope: :area }, 
    numericality: { only_integer: true }
  validates :area, inclusion: { in: @allow_value }
  validates :capacity, numericality: { only_integer: true }
  validates :description, length: { minimum: 10 }
  validates :name, :area, :capacity, :description, presence: true

  has_many :reservations

  def self.search(search)
    where("name LIKE ? OR area LIKE ? OR capacity LIKE ?",
      "%#{search}", "%#{search}", "%#{search}")
  end
end
