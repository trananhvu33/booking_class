module Admin::ApplicationHelper
  def flash_message(key)
    {
      alert: 'alert-danger',
      notice: 'alert-info'
    }[key.to_sym] || key.to_s 
  end
end
