class Admin::ReservationsController < Admin::ApplicationController

  before_action :authenticate_user!
  before_action :teacher_only
  
  def new
    @room = Room.find(params[:room_id])
    @reservation = Reservation.new
  end

  def create
    @reservation = current_user.reservations.build(reservation_params)
    @reservation.room_id = params[:room_id]

    reservations_of_specific_room = Reservation.where(room_id: @reservation.room_id, state: "active")
    date_of_specific_room = reservations_of_specific_room.map { |a| a.date }

    unless date_of_specific_room.include? @reservation.date
      @reservation.save
      redirect_to [:admin, current_user]
    else
      reservations_of_specific_room.each do |reser|
        unless (@reservation.time_start >= reser.time_end || 
          @reservation.time_end <= reser.time_start) && 
          (@reservation.time_start < @reservation.time_end)

          flash.now[:alert] = "Select another time"
          render "new"
          break
        else
          if reser == reservations_of_specific_room.last
            @reservation.save
            redirect_to [:admin, current_user]
          end          
        end
      end
    end

  end

  private

  def reservation_params
    params.require(:reservation).permit(:time_start, :time_end, :date)
  end

  def teacher_only
    if current_user.admin?
      redirect_to root_path, alert: "Only teachers can book classroom"
    end
  end
end
