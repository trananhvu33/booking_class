class Admin::RoomsController < Admin::ApplicationController

  before_action :authorize_admin!

  def new
    @room = Room.new
  end

  def create
    @room = Room.create(room_params)
    if @room.save
      flash[:notice] = "Room has been created."
      redirect_to room_path(@room)
    else
      flash.now[:alert] = "Room has not been created."
      render 'new'
    end
  end

  def edit
    @room = Room.find(params[:id])
  end

  def update
    @room = Room.find(params[:id])
    if @room.update(room_params)
      flash[:notice] = "Room has been updated."
      redirect_to room_path(@room)
    else
      flash[:alert] = "Room has not been updated."
      render 'edit'
    end
  end

  def destroy
    @room = Room.find(params[:id])
    @room.destroy
    flash[:notice] = "Room #{@room.area + @room.name} has been deleted."
    redirect_to rooms_url
  end

  private

  def room_params
    params.require(:room).permit(:area, :name, :capacity, :description)
  end
  
end
