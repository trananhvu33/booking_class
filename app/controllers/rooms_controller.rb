class RoomsController < ApplicationController

  before_action :set_room, only: [:show, :edit, :update, :destroy]

  def index
    @rooms = Room.all
    if params[:search]
      @rooms = Room.search(params[:search]).order("created_at DESC").paginate(:page => params[:page], :per_page => 15)
    else
      @rooms = Room.paginate(:page => params[:page], :per_page => 15)
    end
  end

  

  def show
    # @room = Room.find(params[:id])
  end

  private

  def room_params
    params.require(:room).permit(:area, :name, :capacity, :description)
  end

  def set_room
    @room = Room.find(params[:id])
  end
end
