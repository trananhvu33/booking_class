Rails.application.routes.draw do

  namespace :admin do
    root 'application#index'
    resources :users
    resources :rooms, only: [:new, :create, :edit, :destroy, :update] do 
      resources :reservations
    end
  end

  devise_for :users
  root "rooms#index"
  resources :rooms, only: [:index, :show]
end
