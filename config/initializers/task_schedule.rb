require 'rubygems'
require 'rufus/scheduler'

scheduler = Rufus::Scheduler.new

scheduler.every("10m") do 
  Reservation.where(state: "active").each do |reser|
    if Time.now > reser.time_end 
      reser.inactive!
    end
  end
end