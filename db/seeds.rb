# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
unless User.exists?(email: "admin@example.com")
  User.create!(email: "admin@example.com", password: "password", admin: true)
end

unless User.exists?(email: "user@example.com")
  User.create!(email: "user@example.com", password: "password")
end

100.times do |i|
  puts "Creating #{i}..."
  area = %w{ A B C D E F G H }.sample
  name = rand(100..300)
  capacity = rand(50..100)
  room = Room.create(area: area, name: name, capacity: capacity, description: "this is an example description")
  p room.errors
  puts "Terminated. Error: #{room.errors}"
  puts "Done." if room.persisted?
end