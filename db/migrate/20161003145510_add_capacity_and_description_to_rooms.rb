class AddCapacityAndDescriptionToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :capacity, :integer
    add_column :rooms, :description, :text
  end
end
