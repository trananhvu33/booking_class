class ChangeDateFormatInReservations < ActiveRecord::Migration
  def change
    change_column :reservations, :time_start, :datetime
    change_column :reservations, :time_end, :datetime
  end
end
