class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.references :user, index: true, foreign_key: true
      t.references :room, index: true, foreign_key: true
      t.time :time_start
      t.time :time_end
      t.date :date
      
      t.timestamps null: false
    end
  end
end
